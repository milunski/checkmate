from functools import lru_cache
import logging
import os
from fastapi import Depends, FastAPI, BackgroundTasks
import uvicorn
from src.checks import run_check
from src.config import Settings, render_template
from src.payload import Payload
from src.results import setup_results
from src.utils import greeting, test_connection


SETTINGS = os.getenv("SETTINGS_FILE_PATH")

app = FastAPI()

logger = logging.getLogger(__name__)


@lru_cache()
def get_settings():
    cfg = render_template(SETTINGS)
    return Settings(**cfg)


@app.on_event("startup")
def startup_proc(settings: Settings = get_settings()):
    if os.path.exists(settings.logging["path"]) is False:
        os.mkdir(os.path.dirname(settings.logging["path"]))
    logging.basicConfig(
        filename=settings.logging["path"],
        filemode="w",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=getattr(logging, settings.logging["level"].upper(), None),
    )
    logger.info("Starting server")
    print(greeting())
    print("=== \u26A1 Checking database connectivity \u26A1 ===\n")
    for ping in [settings.source_db, settings.target_db, settings.results_db]:
        test_connection(ping)
    setup_results(settings.results_db)


@app.post("/submit")
async def submission(
    background_tasks: BackgroundTasks,
    payload: Payload,
    setting: Settings = Depends(get_settings),
):
    background_tasks.add_task(run_check, setting=setting, payload=payload)
    return {"message": "Processing"}


if __name__ == "__main__":
    settings = get_settings()
    uvicorn.run("checkmate:app", **settings.uvicorn)
