FROM python:3.8.3

WORKDIR /checkmate
COPY . .
RUN python3 -m pip install -r requirements.txt
RUN pytest -vv test/*.py
