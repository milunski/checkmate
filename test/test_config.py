import os
from unittest import mock
from pydantic import ValidationError
import pytest
from src.config import render_template, Settings


@pytest.fixture(autouse=True)
def mock_source():
    with mock.patch.dict(os.environ, {"SOURCE_SECRET": "fooz"}):
        yield


@pytest.fixture(autouse=True)
def mock_target():
    with mock.patch.dict(os.environ, {"TARGET_SECRET": "bazz"}):
        yield


@pytest.fixture(autouse=True)
def mock_results():
    with mock.patch.dict(os.environ, {"RESULTS_SECRET": "banana"}):
        yield


@pytest.fixture(autouse=True)
def mock_results():
    with mock.patch.dict(os.environ, {"SETTINGS_FILE_PATH": "dummy_config.yaml"}):
        yield


def test_Settings_complete():
    cfg = render_template("./test/test_settings/settings.yaml")
    Settings(**cfg)


def test_Settings_incomplete():
    cfg = render_template("./test/test_settings/settings.yaml")
    Settings(**cfg)


def test_Settings_incomplete():
    # Removed `results_db` from YAML
    cfg = render_template("./test/test_settings/settings_bad.yaml")
    with pytest.raises(ValidationError):
        Settings(**cfg)
