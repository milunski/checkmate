from src.payload import DatabaseObject
from src.utils import create_db_object_path


def test_create_db_object_path():
    valid_db_obj = DatabaseObject(db_name="fizz", schema_name="foo", table_name="bar")
    another_db_object = DatabaseObject(schema_name="foo", table_name="bar")
    one_more_db_object = DatabaseObject(
        db_name=None, schema_name="foo", table_name="bar"
    )
    assert create_db_object_path(one_more_db_object) == "foo.bar"
    assert create_db_object_path(valid_db_obj) == "fizz.foo.bar"
    assert create_db_object_path(another_db_object) == "foo.bar"
