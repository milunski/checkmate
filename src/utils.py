from sqlalchemy import create_engine, engine_from_config
from snowflake.sqlalchemy import URL
from src.payload import DatabaseObject


def greeting():
    return """
                              
    Welcome to checkmate 
    
    
    """


def create_db_object_path(db_object: DatabaseObject) -> str:
    full: str = f"{db_object.db_name}.{db_object.schema_name}.{db_object.table_name}"
    return full.replace("None.", "")


def create_generic_engine(db_setting: dict):
    if "snowflake" in db_setting["sqlalchemy.url"]:
        sqlalchemy_cfg = {k: v for k, v in db_setting.items() if "sqlalchemy." in k}
        stripped_cfg = {
            k.replace("sqlalchemy.", ""): v for k, v in sqlalchemy_cfg.items()
        }
        engine = create_engine(URL(**stripped_cfg))
        return engine
    else:
        engine = engine_from_config(configuration=db_setting)
        return engine


def test_connection(db_setting: dict) -> None:
    """Tests database connections"""
    engine = create_generic_engine(db_setting=db_setting)
    with engine.connect() as conn:
        try:
            conn
            print(f"{db_setting['name']} connection \u2705")
        except Exception:
            print(f"{db_setting['name']} connection \u274C")
    engine.dispose()
