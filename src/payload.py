from typing import List, Optional
from pydantic import BaseModel, StrictStr, ValidationError, validator


class SQLInjectionError(Exception):
    """Custom exception for perceived SQL-I attacks in Payload"""

    def __init__(self, value, message="Possible SQL-I"):
        self.value = value
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"{self.value} ==> {self.message}"


class DatabaseObject(BaseModel):
    db_name: Optional[StrictStr]
    schema_name: Optional[StrictStr]
    table_name: StrictStr

    class Config:
        extra = "forbid"
        anystr_strip_whitespace = True

    @validator("*", pre=True)
    def sql_injection(cls, v):
        if v is not None:
            if ";" in v:
                raise SQLInjectionError(v)
            return v


class Payload(BaseModel):
    source: List[DatabaseObject]
    target: List[DatabaseObject]

    @validator("target", pre=True)
    def equal_length(cls, v, values, **kwargs):
        if len(v) != len(values["source"]):
            raise ValidationError("`source` and `target` fields have unequal tables")
        return v

    class Config:
        extra = "forbid"
        anystr_strip_whitespace = True
