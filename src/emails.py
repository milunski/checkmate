from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import logging
import smtplib
from typing import Union
import pandas as pd
from sqlalchemy import MetaData
from src.config import Settings
from src.payload import DatabaseObject
from src.utils import create_generic_engine, create_db_object_path


logger = logging.getLogger(__name__)


def get_rowcount_fails(setting: Settings, check_id: str) -> Union[pd.DataFrame, int]:
    """Queries for failures based on the check_id"""
    db_path = create_db_object_path(
        DatabaseObject(
            db_name=setting.results_db["sqlalchemy.database"],
            schema_name=setting.results_db["sqlalchemy.schema"],
            table_name=setting.results_db["row_count_table"],
        )
    )
    results_engine = create_generic_engine(setting.results_db)
    MetaData()
    with results_engine.connect() as conn:
        counts = pd.read_sql(
            f"""
            select * 
            from {db_path} 
            where check_id = '{check_id}' and status = 'FAIL'
            """,
            conn,
        )
    if len(counts) == 0:
        return 0
    else:
        return counts


def get_hash_fails(setting: Settings, check_id: str) -> Union[pd.DataFrame, int]:
    """Queries for failures based on the check_id"""
    db_path = create_db_object_path(
        DatabaseObject(
            db_name=setting.results_db["sqlalchemy.database"],
            schema_name=setting.results_db["sqlalchemy.schema"],
            table_name=setting.results_db["hash_table"],
        )
    )
    results_engine = create_generic_engine(setting.results_db)
    MetaData()
    with results_engine.connect() as conn:
        counts = pd.read_sql(
            f"""
            select check_id, source_object, target_object, 'record hashing' as validation, 
            count(status) as failed_records
            from {db_path}
            where check_id = '{check_id}' and status = 'FAIL'
            group by check_id, source_object, target_object
            """,
            conn,
        )
    if len(counts) == 0:
        return 0
    else:
        return counts


def send_email(
    setting: Settings, results: Union[pd.DataFrame, int], check_id: str
) -> None:
    if isinstance(results, int) or len(setting.alerting["email_recipients"]) == 0:
        return None
    message = MIMEMultipart()
    message["Subject"] = f"checkmate Test Results: {check_id[0:10]}"
    message["From"] = setting.alerting["username"]
    results_html = results.to_html(index=False)
    html = f"""
    <html>
    <body style='font-family:Arial'>
    Hello,
    <br>
    <br>
    The tables below have failed checkmate's automated tests.
    <br>
    <br>
    {results_html}
    </html>
    """
    body = MIMEText(html, "html")
    message.attach(body)
    try:
        smtpObj = smtplib.SMTP(setting.alerting["server"], setting.alerting["port"])
        smtpObj.sendmail(
            message["From"], setting.alerting["email_recipients"], message.as_string()
        )
        logging.info(
            f"Alert email successfully sent to {setting.alerting['email_recipients']}"
        )
    except Exception as e:
        logging.error("Alert email not able to be sent")
        logging.error(str(e))
