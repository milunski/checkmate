import os
import sqlite3
import pytest
from src.checks import hash_md5


@pytest.fixture
def valid_data():
    return [
        (1, "2021-12-03", "BTC", 55872.43, 12.4),
        (2, "2021-12-06", "DOGE", 0.25, 100.4),
        (3, "2021-12-06", "ETH", 144.00, 100.4),
    ]


@pytest.fixture
def invalid_data():
    return [
        (1, "2021-12-03", "BTC", 55872.43, 12.4),
        (2, "2021-12-06", "DOGE", 0.25, 100.4),
        (3, "2021-12-08", "ETH", 144.00, 100.9),
    ]


@pytest.fixture
def source_database(valid_data):
    with sqlite3.connect("source.db") as conn:
        cursor = conn.cursor()
        cursor.execute(
            """create table crypto 
            (
                id int PRIMARY KEY, 
                date text,
                symbol text, 
                price real, 
                quantity real
            )
            """
        )
        cursor.executemany("insert into crypto values(?, ?, ?, ?, ?)", valid_data)
    yield conn
    os.remove("source.db")


@pytest.fixture
def target_database(invalid_data):
    with sqlite3.connect("source.db") as conn:
        cursor = conn.cursor()
        cursor.execute(
            """create table crypto 
            (
                id int PRIMARY KEY, 
                date text,
                symbol text, 
                price real, 
                quantity real
            )
            """
        )
        cursor.executemany("insert into crypto values(?, ?, ?, ?, ?)", invalid_data)
    yield conn
    os.remove("source.db")


def test_hash_md5(valid_data, invalid_data):
    for record in zip(valid_data, invalid_data, [True, True, False]):
        assert (hash_md5(record[0]) == hash_md5(record[1])) is record[2]
