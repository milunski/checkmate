from pydantic import ValidationError
import pytest
from src import payload


@pytest.fixture
def observed_single() -> dict:
    return {
        "source": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            }
        ],
        "target": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            }
        ],
    }


@pytest.fixture
def observed_multiple() -> dict:
    return {
        "source": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
        ],
        "target": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
        ],
    }


@pytest.fixture
def observed_unequal_multiple() -> dict:
    return {
        "source": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
        ],
        "target": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            },
        ],
    }


@pytest.fixture
def observed_single_sqli() -> dict:
    return {
        "source": [
            {
                "db_name": "foo; select",
                "schema_name": "bar",
                "table_name": "fizz",
            }
        ],
        "target": [
            {
                "db_name": "foo",
                "schema_name": "bar",
                "table_name": "fizz",
            }
        ],
    }


def test_payload_single(observed_single) -> None:
    assert payload.Payload(**observed_single) == observed_single


def test_payload_multiple(observed_multiple) -> None:
    assert payload.Payload(**observed_multiple) == observed_multiple


def test_payload_unequal_multiple(observed_unequal_multiple):
    with pytest.raises(ValidationError):
        payload.Payload(**observed_unequal_multiple)


def test_payload_single_sqli(observed_single_sqli) -> ValidationError:
    with pytest.raises(payload.SQLInjectionError):
        payload.Payload(**observed_single_sqli)
