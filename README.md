# checkmate

Trying to take the pain out of validating data during migrations. This is a refactoring of the data validation application known as _Concordia_.

## Requirements

- Python >= 3.8.x

Details:  
Developed and tested on Windows 10. CI tests pass on Ubuntu.

__It's recommended that checkmate be used within a virtual environment in order to avoid library dependency issues.__

## Installation and Setup

### Installation

1. From the top-level directory, create a virtual environment (`venv`).
2. Activate the virtual environment.
3. Install packages with `python pip install -r requirements.txt`.

### Dialects

`checkmate` comes with Snowflake and Teradata support included. Users can `pip install` other dialects to accommodate their needs.

### Setup

#### `settings.yaml`

Fill out the `settings.yaml` configuration file. Follow the docs from SQLAlchemy's `engine_from_config` function to adapt `settings.yaml` to your needs.

`settings.yaml` is a Jinja template. Secrets are not stored in plain text within the file, but rather as environment variables that are rendered in the template's expressions.

#### Secrets

Set the database passwords as environment variables with `SOURCE_SECRET`, `TARGET_SECRET`, `RESULTS_SECRET`.

#### Specifying the `settings.yaml` path

Set the directory path to `settings.yaml` with environment variable `SETTINGS_FILE_PATH`.

#### Run the application

Run the application with:
```bash
python checkmate.py --server_yaml=/path/to/settings.yaml
```

### Checking tables

Send `POST` to checkmate's endpoint with the following data.  

Single table to validate. Some platforms like Teradata won't require users to specify a `db_name`.
```json

{
    "source": [
        {
            "db_name": null,
            "schema_name": "POS_T", 
            "table_name": "IRI_DDM_WINE_PRODUCTS"
        }
    ],
    "target": [
        {
            "db_name": "ABI_WH", 
            "schema_name": "POS_T", 
            "table_name": "IRI_DDM_WINE_PRODUCTS"
        }
    ]
}
```

Multiple tables to validate. Source and target tables that are being compared must be in order within the array for valid comparisons.
```json

{
    "source": [
        {
            "db_name": "source1",
            "schema_name": "schema1", 
            "table_name": "table1"
        },
        {
            "db_name": "source2",
            "schema_name": "schema2", 
            "table_name": "table2"
        }
    ],
    "target": [
        {
            "db_name": "target1",
            "schema_name": "schema1", 
            "table_name": "table1"
        },
        {
            "db_name": "target2",
            "schema_name": "schema2", 
            "table_name": "table2"
        }
    ]
    }
```