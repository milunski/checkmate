import datetime
import hashlib
import logging
from typing import AnyStr, List, Tuple
from sqlalchemy import Table
from sqlalchemy.sql import select, text
from sqlalchemy.sql.schema import MetaData
from src.config import Settings
from src.emails import get_hash_fails, get_rowcount_fails, send_email
from src.payload import Payload
from src.results import rowcount_table, tmp_hash, hash_table
from src.utils import create_generic_engine, create_db_object_path


logger = logging.getLogger(__name__)


def hash_md5(record: List[Tuple]) -> AnyStr:
    """Implements MD5 hashing on a record"""
    return hashlib.md5(str(record).encode()).hexdigest()


def run_check(setting: Settings, payload: Payload):
    """Runs all checks"""
    source_engine = create_generic_engine(setting.source_db)
    target_engine = create_generic_engine(setting.target_db)
    results_engine = create_generic_engine(setting.results_db)
    results_metadata = MetaData()
    rowcount_tbl = rowcount_table(
        name=setting.results_db["row_count_table"],
        metadata=results_metadata,
        schema=setting.results_db["sqlalchemy.schema"],
    )
    tmp_source_hash_tbl = tmp_hash(
        name="tmp_source_hash",
        metadata=results_metadata,
        schema=setting.results_db["sqlalchemy.schema"],
    )
    tmp_target_hash_tbl = tmp_hash(
        name="tmp_target_hash",
        metadata=results_metadata,
        schema=setting.results_db["sqlalchemy.schema"],
    )
    results_hash_tbl = hash_table(
        name=setting.results_db["hash_table"],
        metadata=results_metadata,
        schema=setting.results_db["sqlalchemy.schema"],
    )
    tmp_source_hash_tbl.create(bind=results_engine, checkfirst=True)
    tmp_target_hash_tbl.create(bind=results_engine, checkfirst=True)
    check_id: str = hashlib.sha256(str(datetime.datetime.utcnow()).encode()).hexdigest()
    for source_table, target_table in zip(payload.source, payload.target):
        # Record counts and send emails
        with source_engine.connect() as sconn, target_engine.connect() as tconn:
            source_stmt = f"select count(*) from {create_db_object_path(source_table)}"
            target_stmt = f"select count(*) from {create_db_object_path(target_table)}"
            source_count: int = sconn.execute(source_stmt).fetchall()[0][0]
            target_count: int = tconn.execute(target_stmt).fetchall()[0][0]
            count_result = "PASS" if source_count == target_count else "FAIL"
        with results_engine.connect() as rconn:
            insert_statement = rowcount_tbl.insert().values(
                check_id=check_id,
                source_object=create_db_object_path(source_table),
                target_object=create_db_object_path(target_table),
                validation_type="row count",
                source_value=source_count,
                target_value=target_count,
                status=count_result,
            )
            rconn.execute(insert_statement)
        source_engine.dispose()
        target_engine.dispose()
        email_counts = get_rowcount_fails(setting=setting, check_id=check_id)
        send_email(setting=setting, results=email_counts, check_id=check_id)

        # Record hashing
        # Modify a copy of the settings for dynamic database connection
        if "teradata" not in setting.source_db["sqlalchemy.url"]:
            hash_setting = setting.dict()
            hash_setting["source_db"]["sqlalchemy.database"] = source_table.db_name
            source_engine = create_generic_engine(hash_setting["source_db"])
        if "teradata" not in setting.target_db["sqlalchemy.url"]:
            hash_setting = setting.dict()
            hash_setting["target_db"]["sqlalchemy.database"] = target_table.db_name
            target_engine = create_generic_engine(hash_setting["target_db"])
        source_md = MetaData(bind=source_engine, schema=source_table.schema_name)
        target_md = MetaData(bind=target_engine, schema=target_table.schema_name)
        source_tbl = Table(
            source_table.table_name, source_md, autoload_with=source_engine
        )
        target_tbl = Table(
            target_table.table_name, target_md, autoload_with=target_engine
        )
        # Search for primary keys
        source_pk = [
            col.name.lower() for col in source_tbl.primary_key.columns.values()
        ]
        target_pk = [
            col.name.lower() for col in target_tbl.primary_key.columns.values()
        ]
        keys = [
            i
            for i in set(
                source_pk + target_pk + [hash_setting["source_db"]["ordering_key"]]
            )
        ]
        if len(keys) == 0:
            logger.warn(
                f"No keys to order on for {create_db_object_path(source_table)}. Skipping hash validation."
            )
            continue
        # Ensure any key found is present in the tables
        source_columns: set = {i.name for i in source_tbl.columns}
        target_columns: set = {i.name for i in target_tbl.columns}
        source_key = (
            keys[0].upper() if keys[0].upper() in source_columns else keys[0].lower()
        )
        target_key = (
            keys[0].upper() if keys[0].upper() in target_columns else keys[0].lower()
        )
        source_stmt = select(source_tbl).order_by(text(", ".join(keys)))
        target_stmt = select(target_tbl).order_by(text(", ".join(keys)))
        with source_engine.connect() as sconn, target_engine.connect() as tconn, results_engine.connect() as rconn:
            source_result = sconn.execution_options(stream_results=True).execute(
                source_stmt
            )
            for partition in source_result.partitions(1000):
                inserted_records = [
                    {
                        "check_id": check_id,
                        "db_object": create_db_object_path(source_table),
                        "key": row[source_key],
                        "hash": hash_md5(row),
                    }
                    for row in partition
                ]
                rconn.execute(tmp_source_hash_tbl.insert(), inserted_records)
            inserted_records.clear()
            target_result = tconn.execution_options(stream_results=True).execute(
                target_stmt
            )
            for partition in target_result.partitions(1000):
                inserted_records = [
                    {
                        "check_id": check_id,
                        "db_object": create_db_object_path(target_table),
                        "key": row[target_key],
                        "hash": hash_md5(row),
                    }
                    for row in partition
                ]
                rconn.execute(tmp_target_hash_tbl.insert(), inserted_records)
            rconn.execute(
                f"""
                INSERT INTO {results_hash_tbl.name} (server_timestamp, key, 
                    check_id, source_object, target_object, source_hash, 
                    target_hash, status)
                (
                    SELECT  current_timestamp() as server_timestamp, 
                            s.key, s.check_id, s.db_object as source_object, 
                            t.db_object as target_object, s.hash as source_hash, 
                            t.hash as target_hash,
                            CASE 
                                WHEN source_hash = target_hash THEN 'PASS' 
                                ELSE 'FAIL' 
                            END as status
                    FROM    tmp_source_hash s
                    JOIN    tmp_target_hash t
                            ON s.key = t.key
                )
                """
            )
            rconn.execute(f"DROP TABLE {tmp_target_hash_tbl.name}")
            rconn.execute(f"DROP TABLE {tmp_source_hash_tbl.name}")
            # Send notifications for hash checks
            email_hash = get_hash_fails(setting=setting, check_id=check_id)
            send_email(setting=setting, results=email_hash, check_id=check_id)

        source_engine.dispose()
        target_engine.dispose()
