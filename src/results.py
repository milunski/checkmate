from sqlalchemy import Column, Integer, MetaData, String, Table, TIMESTAMP, Sequence
from datetime import datetime
from src.utils import create_generic_engine


def rowcount_table(name: str, metadata: MetaData, schema: str) -> Table:
    """Results table"""
    tbl = Table(
        name,
        metadata,
        Column("id", Integer, Sequence("id_seq"), primary_key=True),
        Column("check_id", String, nullable=False),
        Column(
            "timestamp",
            TIMESTAMP(timezone=False),
            nullable=False,
            default=datetime.utcnow(),
        ),
        Column("source_object", String),
        Column("target_object", String),
        Column("validation_type", String),
        Column("source_value", Integer),
        Column("target_value", Integer),
        Column("status", String),
        schema=schema,
    )
    return tbl


def hash_table(name: str, metadata: MetaData, schema: str) -> Table:
    """Results table"""
    tbl = Table(
        name,
        metadata,
        Column("id", Integer, Sequence("id_seq"), primary_key=True),
        Column("check_id", String, nullable=False),
        Column(
            "server_timestamp",
            TIMESTAMP(timezone=False),
            nullable=False,
            default=datetime.utcnow(),
        ),
        Column("key", String),
        Column("source_object", String),
        Column("target_object", String),
        Column("source_hash", String),
        Column("target_hash", String),
        Column("status", String),
        schema=schema,
    )
    return tbl


def tmp_hash(name: str, metadata: MetaData, schema: str) -> Table:
    """Temp hash table for intermediate values"""
    tbl = Table(
        name,
        metadata,
        Column("id", Integer, Sequence("id_seq"), primary_key=True),
        Column("check_id", String, nullable=False),
        Column(
            "timestamp",
            TIMESTAMP(timezone=False),
            nullable=False,
            default=datetime.utcnow(),
        ),
        Column("db_object", String),
        Column("key", String),
        Column("hash", String),
        schema=schema,
    )
    return tbl


def setup_results(db_settings: dict):
    engine = create_generic_engine(db_settings)
    metadata_obj = MetaData(engine)
    engine.execute(
        f"CREATE DATABASE IF NOT EXISTS {db_settings['sqlalchemy.database']}"
    )
    engine.execute(f"CREATE SCHEMA IF NOT EXISTS {db_settings['sqlalchemy.schema']}")
    rowcount_tbl = rowcount_table(
        db_settings["row_count_table"], metadata_obj, db_settings["sqlalchemy.schema"]
    )
    hash_tbl = hash_table(
        db_settings["hash_table"], metadata_obj, db_settings["sqlalchemy.schema"]
    )
    try:
        rowcount_tbl.create(checkfirst=True)
        print(f"Record counts: {rowcount_tbl.fullname}")
        hash_tbl.create(checkfirst=True)
        print(f"Record hash: {hash_tbl.fullname}")
    except Exception as e:
        print(str(e))
    finally:
        engine.dispose()
