import os
from typing import Any, Mapping, Optional
from jinja2 import Environment, FileSystemLoader, select_autoescape
from pydantic import BaseSettings
import yaml


def render_template(file_path: str) -> Mapping[Any, Any]:
    env = Environment(loader=FileSystemLoader("./"), autoescape=select_autoescape())
    cfg_template = env.get_template(file_path).render(
        SOURCE_SECRET=os.getenv("SOURCE_SECRET"),
        TARGET_SECRET=os.getenv("TARGET_SECRET"),
        RESULTS_SECRET=os.getenv("RESULTS_SECRET"),
    )
    cfg = yaml.load(cfg_template, Loader=yaml.SafeLoader)
    return cfg


class Settings(BaseSettings):
    source_db: Mapping[Any, Any]
    target_db: Mapping[Any, Any]
    results_db: Mapping[Any, Any]
    uvicorn: Mapping[Any, Any]
    alerting: Optional[Mapping[Any, Any]]
    logging: Optional[Mapping[Any, Any]]
